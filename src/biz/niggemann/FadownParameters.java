package biz.niggemann;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import static biz.niggemann.FadownFileUtility.getTempDir;
import static biz.niggemann.FadownUtility.logStackTrace;
import static biz.niggemann.FadownUtility.stopAllInstances;

class FadownParameters {

    // Constants
    private String flashAirURL = "http://FLASHAIR/DCIM";
    private String destinationFolder = getTempDir() + "FA_IMGs";
    private String fileTypes = "JPG";
    private String imageFilenamesToShow = "";
    private String backupFolderList = "";
    private String pngOverlayFilename = "";
    private String processFileTypes = "JPG";
    private String externalProgram = "";
    private int pauseTime = 0;
    private int waitAfterDownloadTime = 0;
    private int logIt = 0;
    private int detectionMaxSize = -1;
    private boolean detectFacesOnly = false;
    private boolean runContinuously = true;
    private boolean showLastImageInBrowser = false;
    private boolean showAllImagesInBrowser = false;
    private boolean generateDetectionOnly = false;
    private boolean downloadParallel = false;

    // Constructor
    FadownParameters(String[] args) {
        List<String> argList = Arrays.asList(args);
        Map<String, List<String>> parameters = mapCmdArgs(argList);
        String configFile = "FADown.cfg";
        configFile = parseOneParameter(parameters, "k", "config", configFile, false);

        if ((configFile.length() > 0) && (new File(configFile).exists()))
            parameters = mapCmdArgs(getParametersFromFile(configFile)); // Set new command line arguments

        parseAllParameters(parameters);
    }

    // Show help screen
    private void showHelp() {
        System.out.println("FADown");
        System.out.println("Version 1.3 by Andreas Niggemann");
        System.out.println("Usage: FADown [options...]");
        // Available option letters: i,j,m,z
        System.out.println("-u, --url <url>       URL of the FlashAir card");
        System.out.println("-d, --destination-directory <directory> Destination directory");
        System.out.println("-o, --backup-folders  <folder names list> Backup files to folders, comma separated list");
        System.out.println("-f, --filetypes <file extension list> Comma separated list of file extensions");
        System.out.println("-n, --png-overlay <png filename> Overlay every image with the png image");
        System.out.println("-e, --handle <file extension list> Process (view, detection, overlay) only file extensions, comma separated list");
        System.out.println("-t, --external-program <program and parameter list> Start external program, image filename will be added as last parameter");
        System.out.println("-c, --continuously    Run continuously, waiting for more files");
        System.out.println("-r, --parallel        Parallel download files");
        System.out.println("-p, --pause <seconds> Wait seconds before try again in continuous mode");
        System.out.println("-w, --wait <seconds>  Wait seconds after downloading a file to prevent overheating");
        System.out.println("-a, --all             Show images in browser");
        System.out.println("-b, --browser         Show last image in browser");
        System.out.println("-l, --log             Log transfered filenames on console");
        System.out.println("-x, --xlog            Extended logging on console");
        System.out.println("-s, --stop            Stop all instances of FADown on this computer");
        System.out.println();
        System.out.println("-v, --view <filename list> Show image files, comma separated list");
        System.out.println();
        System.out.println("Face/eye detection options (openCV must be installed):");
        System.out.println("-q, --faces <pixel>   Show faces in separate images of pixel size");
        System.out.println("-y, --eyes <pixel>    Show eyes in separate images of pixel size");
        System.out.println("-g, --generate-detection Do not show detected features images, only generate them");
        System.out.println();
        System.out.println("-k, --config <filename> Load parameters from config file (Default: FADown.cfg)");
        System.out.println();
        System.out.println("-h, --help            This help");
        System.out.println();
        String defaultFolder = getTempDir() + "FA_IMGs";
        System.out.println("If no options are set, FADown starts with -u FLASHAIR -d " + defaultFolder + " -f " + fileTypes + " -l -c");
        System.out.println();
        flashAirURL = "";
        destinationFolder = "";
    }

    private List<String> getParametersFromFile(String configFilename) {
        List<String> newCmdArgs = new ArrayList<>();

        try (Scanner scanner = new Scanner(new File(configFilename))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine().trim();
                if (line.length() > 0) {
                    String[] lineElements = line.split("[\\s=:]+", 2);
                    if (lineElements.length >= 1)
                        newCmdArgs.add("-" + lineElements[0]);
                    if (lineElements.length > 1)
                        newCmdArgs.add(lineElements[1]);
                }
            }
        } catch (FileNotFoundException e) {
            logStackTrace(e);
        }

        return newCmdArgs;
    }

    private void parseAllParameters(Map<String, List<String>> params) {
        if (params.size() > 0) {
            if (parseOneParameter(params, "s", "stop", "", true).equals("TRUE"))
                stopAllInstances();
            else if (parseOneParameter(params, "h", "help", "", true).equals("TRUE"))
                showHelp();
            else {
                flashAirURL = parseOneParameter(params, "u", "url", "http://FLASHAIR", false) + "/DCIM";
                destinationFolder = parseOneParameter(params, "d", "destination-directory", destinationFolder, false);
                backupFolderList = parseOneParameter(params, "o", "backup-folders", "", false);
                fileTypes = parseOneParameter(params, "f", "filetypes", "jpg", false).toUpperCase();
                processFileTypes = parseOneParameter(params, "e", "handle", "jpg", false).toUpperCase();
                pngOverlayFilename = parseOneParameter(params, "n", "png-overlay", "", false);
                externalProgram = parseOneParameter(params, "t", "external-program", "", false);
                pauseTime = Integer.parseInt(parseOneParameter(params, "p", "pause", "0", false));
                waitAfterDownloadTime = Integer.parseInt(parseOneParameter(params, "w", "wait", "0", false));
                setRunContinuously(parseOneParameter(params, "c", "continuously", "", true).equals("TRUE"));
                downloadParallel = (parseOneParameter(params, "r", "parallel", "", true).equals("TRUE"));
                showLastImageInBrowser = (parseOneParameter(params, "b", "browser", "", true).equals("TRUE"));
                showAllImagesInBrowser = (parseOneParameter(params, "a", "all", "", true).equals("TRUE"));
                generateDetectionOnly = (parseOneParameter(params, "g", "generate-detection", "", true).equals("TRUE"));
                logIt = (parseOneParameter(params, "l", "log", "", true).equals("TRUE")) ? 1 : 0;
                logIt = (parseOneParameter(params, "x", "xlog", "", true).equals("TRUE")) ? 2 : logIt;
                imageFilenamesToShow = parseOneParameter(params, "v", "view", "", false);
                showLastImageInBrowser = (imageFilenamesToShow.length() > 0) || showLastImageInBrowser;
                getDetectionMaxSize(params);
            }
        } else { // Automatic configuration because of missing parameters
            logIt = 1;
            System.out.println("FADown transfers " + fileTypes + " from " + flashAirURL + " to " + destinationFolder);
        }
    }

    private void getDetectionMaxSize(Map<String, List<String>> params) {
        detectFacesOnly = false;

        try {
            detectionMaxSize = Integer.parseInt(parseOneParameter(params, "q", "faces", "-1", false));
            if (detectionMaxSize > 0)
                detectFacesOnly = true;
            else
                detectionMaxSize = Integer.parseInt(parseOneParameter(params, "y", "eyes", "-1", false));
        } catch (NumberFormatException ex) {
            detectionMaxSize = 900;
        }
    }

    private String parseOneParameter(Map<String, List<String>> params, String shortName, String
            longName, String defaultValue, boolean switchOnly) {
        String retVal;

        if (switchOnly) {
            retVal = (params.containsKey(shortName) || params.containsKey(longName)) ? "TRUE" : "";
        } else { // Params with extra values
            if (params.containsKey(shortName) && (!params.get(shortName).isEmpty()))
                retVal = params.get(shortName).get(0);
            else if (params.containsKey(longName) && (!params.get(longName).isEmpty()))
                retVal = params.get(longName).get(0);
            else
                retVal = defaultValue;
        }

        return retVal;
    }

    private Map<String, List<String>> mapCmdArgs(List<String> cmdArgs) {
        Map<String, List<String>> params = new HashMap<>();
        List<String> options = null;
        String errorMessage = "";

        for (String a : cmdArgs) {
            a = a.replace("--", "-"); // double option hyphen allowed

            if (a.charAt(0) == '-') {
                if (a.length() >= 2) {
                    options = new ArrayList<>();
                    params.put(a.substring(1), options);
                } else
                    errorMessage = "Error at argument " + a;
            } else if (options != null) {
                options.add(a);
            } else
                errorMessage = "Illegal parameter usage";

            if (!errorMessage.equals("")) {
                System.out.println(errorMessage);
                params.clear();
                break;
            }
        }

        return params;
    }

    String getFlashAirURL() {
        return flashAirURL;
    }

    String getDestinationFolder() {
        return destinationFolder;
    }

    String getFileTypes() {
        return fileTypes;
    }

    String getProcessFileTypes() {
        return processFileTypes;
    }

    int getPauseTime() {
        return pauseTime;
    }

    int getWaitAfterDownloadTime() {
        return waitAfterDownloadTime;
    }

    int getLogIt() {
        return logIt;
    }

    boolean isRunContinuously() {
        return runContinuously;
    }

    void setRunContinuously(boolean runContinuously) {
        this.runContinuously = runContinuously;
    }

    boolean isDownloadParallel() {
        return downloadParallel;
    }

    boolean isShowLastImageInBrowser() {
        return showLastImageInBrowser;
    }

    boolean isShowAllImagesInBrowser() {
        return showAllImagesInBrowser;
    }

    String getImageFilenamesToShow() {
        return imageFilenamesToShow;
    }

    String getBackupFolderList() {
        return backupFolderList;
    }

    String getPngOverlayFilename() {
        return pngOverlayFilename;
    }

    String getExternalProgram() {
        return externalProgram;
    }

    int getDetectionMaxSize() {
        return detectionMaxSize;
    }

    boolean isDetectFacesOnly() {
        return detectFacesOnly;
    }

    boolean isGenerateDetectionOnly() {
        return generateDetectionOnly;
    }

}
