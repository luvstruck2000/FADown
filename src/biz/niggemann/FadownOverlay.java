package biz.niggemann;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.exif.ExifIFD0Directory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static biz.niggemann.FadownUtility.logStackTrace;

class FadownOverlay {

    private static BufferedImage overlayImage;

    private FadownOverlay() {
        throw new IllegalStateException("Utility class");
    }

    static void overlayStart(FadownParameters p) {
        final String overlayImageFilename = p.getPngOverlayFilename();
        if (overlayImageFilename.length() > 0) // Load overlay image from PNG file
            try {
                File overlayImageFile = new File(overlayImageFilename);
                if (overlayImageFile.exists())
                    overlayImage = ImageIO.read(overlayImageFile);
            } catch (IOException e) {
                logStackTrace(e);
            }
    }

    private static BufferedImage rotateImage(BufferedImage imageToRotate, double angle) {
        double rads = Math.toRadians(angle);
        double sin = Math.abs(Math.sin(rads));
        double cos = Math.abs(Math.cos(rads));
        int w = imageToRotate.getWidth();
        int h = imageToRotate.getHeight();
        int newWidth = (int) Math.floor(w * cos + h * sin);
        int newHeight = (int) Math.floor(h * cos + w * sin);
        BufferedImage rotated = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = rotated.createGraphics();
        AffineTransform at = new AffineTransform();
        at.translate((newWidth - w) / 2d, (newHeight - h) / 2d);
        at.rotate(rads, w / 2d, h / 2d);
        g2d.setTransform(at);
        g2d.drawImage(imageToRotate, 0, 0, null);
        g2d.dispose();
        return rotated;
    }

    private static int getExifOrientation(File originalFile) {
        int orientation = 1;
        try {
            ExifIFD0Directory exifDir = ImageMetadataReader.readMetadata(originalFile).getFirstDirectoryOfType(ExifIFD0Directory.class);
            if ((exifDir != null) && (exifDir.containsTag(ExifIFD0Directory.TAG_ORIENTATION))) {
                orientation = exifDir.getInt(ExifIFD0Directory.TAG_ORIENTATION);
            }
        } catch (IOException | com.drew.imaging.ImageProcessingException | com.drew.metadata.MetadataException e) {
            orientation = 9999; // Suppress any metadata exception
        }
        return orientation;
    }

    static void overlayImageWithPng(String originalFilename) {
        if ((overlayImage != null) && (overlayImage.getHeight() > 0) && (overlayImage.getWidth() > 0))
            try {
                File originalFile = new File(originalFilename);

                double rotationFactor = getRotationFactor(getExifOrientation(originalFile));

                BufferedImage image = ImageIO.read(originalFile);
                if (rotationFactor != 0d)
                    image = rotateImage(image, rotationFactor);

                BufferedImage combined = new BufferedImage(overlayImage.getWidth(), overlayImage.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics g = combined.getGraphics();
                g.drawImage(image, 0, 0, null);
                g.drawImage(overlayImage, 0, 0, null);

                int i = originalFilename.lastIndexOf('.');
                String extension = (i > 1) ? originalFilename.substring(i + 1) : "";
                ImageIO.write(combined, extension, originalFile);
            } catch (IOException e) {
                logStackTrace(e);
            }
    }

    private static double getRotationFactor(int exifOrientation) {
        double retVal = 0d;
        boolean overlayHasPortraitFormat = overlayImage.getHeight() > overlayImage.getWidth();

        switch (exifOrientation) {
            case 1:
            case 6:
                if (overlayHasPortraitFormat)
                    retVal = 90d;
                break;
            case 3:
                if (overlayHasPortraitFormat)
                    retVal = 180d;
                break;
            case 8:
                if (overlayHasPortraitFormat)
                    retVal = -90d;
                break;
            case 9999: // Exception (Error) will be ignored
                break;
            default:
                break;
        }

        return retVal;
    }

}
