package biz.niggemann;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Pattern;

import static biz.niggemann.FadownFileUtility.getErrorSignal;
import static biz.niggemann.FadownFileUtility.getExitSignal;
import static biz.niggemann.FadownUtility.*;

class FadownHtmlProcessing {

    private FadownHtmlProcessing() {
        throw new IllegalStateException("FadownUtility class");
    }

    // Read HTML page from URL
    static String readHtmlFromUrl(String readThisURL) {
        String retVal = getExitSignal();

        if (!exitWasSignaled()) {
            try {
                URL urlToRead = new URL(readThisURL);
                logItOut("Read URL: " + readThisURL, 2);
                URLConnection c = urlToRead.openConnection();
                c.setConnectTimeout(5000);
                c.setReadTimeout(10000);

                try (BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()))) {
                    String inputLine;
                    StringBuilder sb = new StringBuilder();
                    while ((inputLine = in.readLine()) != null)
                        sb.append(inputLine).append("\n");
                    retVal = sb.toString();
                }

            } catch (IOException e) {
                notConnectedErrorMessage();
                retVal = getErrorSignal();
            }
        }

        return retVal;
    }

    private static String processHTMLLine(String htmlLine, String prefix, String fitypes) {
        String retVal = "";
        final String[] lparts = htmlLine.split("\"");

        if (lparts.length > 7) {
            String element = lparts[7].trim();
            if (element.length() > 0) {
                String[] fnameParts = element.split(Pattern.quote(".")); // Get filename and file type
                String ext = fnameParts[fnameParts.length - 1].toUpperCase(); // file type
                if ((fnameParts.length == 1) || (fitypes.length() < 2) || (fitypes.contains(ext + ","))) // file type OK
                    retVal = prefix + element + ",";
            }
        }

        return retVal;
    }

    // Extract the file list from the HTML string
    static String getFileListFromHtml(String htmls, String prefix, String fitypes) {
        String retVal;
        StringBuilder sb = new StringBuilder();
        final String[] htmlLines = htmls.split("\\n");

        for (String line : htmlLines) {
            if (line.contains("/DCIM") && !line.contains("__TSB")) // photo folder or photo file name
                sb.append(processHTMLLine(line, prefix, fitypes));
        }

        retVal = sb.toString();
        retVal = (retVal.length() > 1) ? retVal.substring(0, retVal.length() - 1) : ""; // Delete last comma
        logItOut("List from HTML: " + retVal, 2);

        return retVal;
    }
}
