package biz.niggemann;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import static biz.niggemann.FadownUtility.logItOut;
import static biz.niggemann.FadownUtility.logStackTrace;

class FadownFileUtility {

    private static final String SEMAPHORE_FILE = endsPathWithFileSeparator(System.getProperty("java.io.tmpdir")) + "FADown.stop";
    private static final String ERROR_SIGNAL = "ERROR";
    private static final String EXIT_SIGNAL = "EXIT";
    private static final String OK_SIGNAL = "OK";

    static String lastImageFilename = "";
    private static FadownFaceFeaturesDetection faceFeaturesDetection;

    private FadownFileUtility() {
        throw new IllegalStateException("Utility class");
    }

    static void fileUtilityStart(FadownParameters p) {
        if (p.getDetectionMaxSize() > 0) // Load everything for openCV face features detection
            faceFeaturesDetection = new FadownFaceFeaturesDetection(p.getDetectionMaxSize(), p.isDetectFacesOnly(), p.isGenerateDetectionOnly());
    }

    static void showImageFileConditionally(boolean condition, String imageFilename) {
        if (condition && (imageFilename.length() > 0)) {
            if (faceFeaturesDetection != null)
                faceFeaturesDetection.showFaceFeatures(imageFilename);
            showImageFile(imageFilename);
        }
        lastImageFilename = "";
    }

    static void showImageFile(String imageFilename) {
        try {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                URI fileURI = new URI("file:///" + imageFilename.replace("\\", "/"));
                Desktop.getDesktop().browse(fileURI);
            }
        } catch (URISyntaxException | IOException e) {
            logStackTrace(e);
        }
    }

    static boolean fileExtensionMatchesProcessList(FadownParameters p, String filename) {
        boolean retVal = true;
        if (p.getProcessFileTypes().length() > 0) {
            String extension = filename.substring(filename.lastIndexOf('.') + 1).toUpperCase();
            retVal = (p.getProcessFileTypes() + ',').contains(extension + ',');
        }
        return retVal;
    }

    static void makeBackups(String fileToWrite, List<String> folderList) {
        folderList.parallelStream().forEach(folderName -> {
            folderName = (folderName.endsWith(File.separator)) ? folderName : folderName + File.separator;

            if ((folderName.length() > 1) && (destinationDirectoryExists(folderName))) {
                try {
                    String targetFileName = folderName + new File(fileToWrite).getName();
                    Path target = Paths.get(targetFileName);
                    Path originalPath = Paths.get(fileToWrite);
                    Files.copy(originalPath, target, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) { // Suppress Exceptions
                }
            }
        });
    }

    // Force creation of destination directory
    static boolean destinationDirectoryExists(String directoryName) {
        boolean retVal = false;
        directoryName += File.separator;
        directoryName = directoryName.replace(File.separator + File.separator, File.separator); // eliminate double file separators
        File dir = new File(directoryName);

        if (!dir.exists()) // Create destination directory if not there
            try {
                logItOut("Creating destination folder " + directoryName, 2);
                Files.createDirectories(Paths.get(directoryName));
            } catch (IOException e) {
                logStackTrace(e);
            }

        if (!dir.exists()) // Still destination directory not there -> Error
            logItOut("Destination folder " + directoryName + " does not exist and could not be created", 0);
        else
            retVal = true;

        return retVal;
    }

    static String removeDoubleSlashes(String path) {
        String retVal = path.replace("//", "/");
        return retVal.replace("http:/", "http://");
    }

    private static String endsPathWithFileSeparator(String path) {
        return (path.endsWith(File.separator)) ? path : path + File.separator;
    }

    static String getTempDir() {
        return endsPathWithFileSeparator(System.getProperty("java.io.tmpdir"));
    }

    static String getSemaphoreFile() {
        return SEMAPHORE_FILE;
    }

    static String getErrorSignal() {
        return ERROR_SIGNAL;
    }

    static String getOkSignal() {
        return OK_SIGNAL;
    }

    static String getExitSignal() {
        return EXIT_SIGNAL;
    }


}
